window.onload = function() {
	Crafty.init(600, 600);
    Crafty.background("black");
    Crafty.sprite(50, "images/runningdude.jpg", {
        running00: [0,0],
        running01: [0,1],
        running02: [0,2],
        running03: [0,3],
        running10: [1,0],
        running11: [1,1]
    });
	//Crafty.canvas.init();

	//Crafty.e("Wall, 2D, DOM, Color, Collision")
	//	.color("rgb(40,40,40)")
	//	.attr({x: 500, y: 500, w:200, h:100,z: 1});

	//Crafty.e("Wall, 2D, DOM, Color, Collision")
	//	.color("rgb(40,40,40)")
	//	.attr({x: 200, y: 300, w:200, h:20, z:1});

	var player = Crafty.e("Player, 2D, DOM, Color , Multiway, Collision, running00")
		.attr({x:0, y:0, w:50, h:50, z: 2})
		.color("rgb(0,0,0)")
		.multiway(3.3,{UP_ARROW: -90, DOWN_ARROW: 90, RIGHT_ARROW: 0, LEFT_ARROW: 180})
		.onHit("Wall", function(entities) {
			for(var i in entities){
				var depth = getDepth(this, entities[i].obj);
				if(depth){
					if(Math.abs(depth.x) > Math.abs(depth.y)){
						this.y += depth.y;
					}else if(Math.abs(depth.y) > Math.abs(depth.x) ){
						this.x += depth.x;
					}
				}
			}
		})
		.bind("EnterFrame", function() {
			var rect = {
				_x: this.x + (this.w/2), 
				_y: this.y + (this.h/2), 
				_w: 1, 
				_h: 1};
			var entities = _.find(Crafty.map.search( rect ),function(entity){ return entity.has("Tile")});
			//We have just moved to a new Tile
			//Remove Ones that are out of range                
            if (entities == null || this.centerOnTile == undefined || entities[0] !== this.centerOnTile){
				if(entities != null)
                    this.attr({centerOnTile: entities[0]});
				this.attr({			rect: {
					_x: (parseInt((this._x + this._w/2)/64)-6)*64, 
					_y: (parseInt((this._y + this._h/2 )/64)-6)*64, 
					_w: 64*11, 
					_h: 64*11}

					});
                
                if( 0 > this._x){
                    this.rect._x =(parseInt((this._x + this._w/2 )/64)-6)*64;
                }if ( 0 > this._y){
                    this.rect._y = (parseInt((this._y + this._h/2 )/64)-6)*64;
                }
				var visibleE = Crafty.map.search( this.rect, false );
				
				var allE = Crafty("DOM").toArray();
				
				//var notVisibleE = _.difference(allE, visibleE);			
				

				for( var i = this.rect._x;  i <= this.rect._x + this.rect._w; i = i+64){
					for(var j = this.rect._y; j <= this.rect._y + this.rect._h; j = j+64){
                        //console.log(i + ":" + j );
                        var tile = _.find(visibleE,function(element){return element._x == i && element._y == j} );
                        if( tile === null || tile === undefined ){
							
							Crafty.e("Tile, 2D, DOM, Color")
								.color("rgb("+Crafty.math.randomInt(0,255) +","+Crafty.math.randomInt(0,255)+","+Crafty.math.randomInt(0,255)+")") 
								.attr({x: i,y: j ,w: 64,h: 64,z: 1});
						}else if (tile.__c.Tile == true && tile.__c.DOM ==undefined ){

                            
                            
                               tile.addComponent("DOM").draw();
                            
                                
                            
							
						}

					}
				}



				for(var i=0; i< allE.length; i++){
					var e = Crafty(allE[i]);
					var en = _.contains(visibleE, e);
					if(!en && e != undefined){
						try{
						e.removeComponent("DOM");
						}catch(err){}
						//try{
							//e.destroy();
						//}catch(err){}

					}
				}
				
				
				//populateAroundPlayer(this);
                
			}
		});


	function getDepth(entity1, entity2){
		halfW1 = entity1.w/2;
		halfH1 = entity1.h/2;
		halfW2 = entity2.w/2;
		halfH2 = entity2.h/2;

		var center1 = new Crafty.math.Vector2D(entity1.x + halfW1, entity1.y + halfH1);
		var center2 = new  Crafty.math.Vector2D(entity2.x + halfW2, entity2.y + halfH2);

		var distanceX = center1.x - center2.x;
		var distanceY = center1.y - center2.y;
		var minDistanceX = halfW1 + halfW2;
		var minDistanceY = halfH1 + halfH2;

		if (Math.abs(distanceX) >= minDistanceX || Math.abs(distanceY) >= minDistanceY)
			return new Crafty.math.Vector2D();
		
		var depthX = distanceX > 0 ? minDistanceX - distanceX : -minDistanceX - distanceX;
		var depthY = distanceY > 0 ? minDistanceY - distanceY : -minDistanceY - distanceY;
		return new Crafty.math.Vector2D(depthX, depthY);
	}
	
	Crafty.viewport.clampToEntities = false;
	Crafty.viewport.follow(player, 0, 0);
}
